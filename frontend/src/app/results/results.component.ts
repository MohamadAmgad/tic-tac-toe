import { Component, OnInit } from '@angular/core';
import { AddplayersService } from '../addplayers.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  results: Object;

  constructor(private playerService: AddplayersService) { }

  ngOnInit() {
    this.playerService.showResults().subscribe(
       res => {
        this.results = res;
      });
  }

}
