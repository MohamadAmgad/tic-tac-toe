import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';





import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { CellComponent } from './cell/cell.component';
import { PlayComponent } from './play/play.component';
import { AddplayersService } from './addplayers.service';
import { ResultsComponent } from './results/results.component';
import { LandingpageComponent } from './landingpage/landingpage.component';


const routes: Routes = [
  {
    path: 'board',
    component: BoardComponent
  },
  {
    path: 'play',
    component: PlayComponent
  },
  {
    path: 'results',
    component: ResultsComponent
  },
  {
    path: '',
    component: LandingpageComponent
  }

];




@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    CellComponent,
    PlayComponent,
    ResultsComponent,
    LandingpageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [AddplayersService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
