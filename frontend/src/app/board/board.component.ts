import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AddplayersService } from '../addplayers.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  constructor(private cookieService: CookieService, private playerService: AddplayersService) { }
  p1name = this.cookieService.get('player1');
  p2name = this.cookieService.get('player2');
  board = Array(9).fill(null);
  player = 'X';
  winner = "";
  gamewinner = false;


  get winnerName(){
    if (this.winner != ""){
      if(this.winner == 'X'){
        if(!this.gamewinner){
          this.playerService.addPlayer({p1name:this.p1name, p2name:this.p2name, winner:this.p1name})
        }
        this.gamewinner = true;
        return `${this.p1name} is the winner`
      }
      else{
        if(!this.gamewinner){
          this.playerService.addPlayer({p1name:this.p1name, p2name:this.p2name, winner:this.p2name})
        }
        this.gamewinner = true;
        return `${this.p2name} is the winner`
      }
    }
    else{
      console.log(this.containsNull())
      if(!this.containsNull()){
        if(!this.gamewinner){
          this.playerService.addPlayer({p1name:this.p1name, p2name:this.p2name, winner:"No Winner"})
        }
        this.gamewinner = true;
        return `Draw`;
      }
      if(this.player == 'X'){
        return `${this.p1name}'s turn`;
      }
      else{
        return `${this.p2name}'s turn`;
      }
    }
  }

  containsNull(){
    for(let i = 0; i<9;i++){
      if(this.board[i] == null){
        return true;
      }
    }
    return false;
  }


  restartGame() {
    this.board = Array(9).fill(null);
    this.player = 'X';
    this.winner = "";
    this.gamewinner = false;
  }

  winnerMove() {
    const winnerPlaces = [
      [0, 1, 2], [3, 4, 5], [6, 7, 8],
      [0, 3, 6], [1, 4, 7], [2, 5, 8],
      [0, 4, 8], [2, 4, 6]
    ];
    for (let winnerplace of winnerPlaces) {
        if ( this.board[winnerplace[0]] != null
            && this.board[winnerplace[0]] === this.board[winnerplace[1]]
            && this.board[winnerplace[1]] === this.board[winnerplace[2]]) {
              return true;
        }
    }
    return false;

  }

  moves(i) {
    if(this.winner == "" && this.board[i] == null ){
      this.board[i] = this.player;
      if(this.winnerMove()) {
        this.winner = this.player;
      }
      else if(this.player === 'X'){
        this.player = 'O';
      }
      else{
        this.player = 'X'
      }
    }
  }

  ngOnInit() {
  }

}
