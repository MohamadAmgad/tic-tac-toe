import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddplayersService {
  url = 'http://localhost:4000';
  constructor(private http: HttpClient) {}

  addPlayer(results){
    this.http.post(`${this.url}/add`, results).subscribe(res => console.log('Done'));
  }
  showResults(){
    return this.http.get(`${this.url}/results`);
  }

}
