import { TestBed, inject } from '@angular/core/testing';

import { AddplayersService } from './addplayers.service';

describe('AddplayersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddplayersService]
    });
  });

  it('should be created', inject([AddplayersService], (service: AddplayersService) => {
    expect(service).toBeTruthy();
  }));
});
