import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { AddplayersService } from '../addplayers.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';





@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit {

  constructor(private cookieService: CookieService, private playerService: AddplayersService,private router: Router) { }

  ngOnInit() {
    // this.playerService.showResults();
  }

  registerPlayers(user){
    var player1 = user.p1name;
    var player2 = user.p2name;
    this.cookieService.set('player1', player1);
    this.cookieService.set('player2', player2);
    this.router.navigate(['./board']);
  }

}
