const express = require('express');
const app = express();
const resultsRoutes = express.Router();


let Results = require('../models/results');


resultsRoutes.route('/add').post(function (req, res) {
  console.log(req.body)
  let result = new Results(req.body);
  result.save()
    .then(game => {
    res.status(200).json({'result': 'Result are added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});

resultsRoutes.route('/results').get(function (req, res) {
    Results.find(function (err, results){
    if(err){
      console.log(err);
    }
    else {
      res.json(results);
    }
  });
});

module.exports = resultsRoutes;
