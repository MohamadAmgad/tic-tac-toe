const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Results = new Schema({
  p1name: {
    type: String,
    required: true
  },
  p2name: {
    type: String,
    required: true
  },
  winner: {
    type: String,
    required: true
  }
},{
    collection: 'results'
});

module.exports = mongoose.model('results', Results);
